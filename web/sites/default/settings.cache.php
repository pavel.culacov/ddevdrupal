<?php
$settings['memcache']['servers'] = ['memcached:11211' => 'default'];
$settings['memcache']['bins'] = ['default' => 'default'];
$settings['memcache']['key_prefix'] = 'ddevdrupal_';
// Prepare drupal so we can use memcache even before memcache module is
// enabled or initiated.
$memcache_exists = class_exists('Memcache', FALSE);
$memcached_exists = class_exists('Memcached', FALSE);

if ($memcache_exists || $memcached_exists) {
  $class_loader->addPsr4('Drupal\\memcache\\', 'modules/contrib/memcache/src');
  // Define custom bootstrap container definition to use Memcache for cache.container.
  $settings['bootstrap_container_definition'] = [
    'parameters' => [],
    'services' => [
      # Dependencies.
      'settings' => [
        'class' => 'Drupal\Core\Site\Settings',
        'factory' => 'Drupal\Core\Site\Settings::getInstance',
      ],
      'memcache.settings' => [
        'class' => 'Drupal\memcache\MemcacheSettings',
        'arguments' => ['@settings'],
      ],
      'memcache.factory' => [
        'class' => 'Drupal\memcache\Driver\MemcacheDriverFactory',
        'arguments' => ['@memcache.settings'],
      ],
      'memcache.timestamp.invalidator.bin' => [
        'class' => 'Drupal\memcache\Invalidator\MemcacheTimestampInvalidator',
        # Adjust tolerance factor as appropriate when not running memcache on localhost.
        'arguments' => ['@memcache.factory', 'memcache_bin_timestamps', 0.001],
      ],
      'memcache.timestamp.invalidator.tag' => [
        'class' => 'Drupal\memcache\Invalidator\MemcacheTimestampInvalidator',
        # Remember to update your main service definition in sync with this!
        # Adjust tolerance factor as appropriate when not running memcache on localhost.
        'arguments' => ['@memcache.factory', 'memcache_tag_timestamps', 0.001],
      ],
      'memcache.backend.cache.container' => [
        'class' => 'Drupal\memcache\DrupalMemcacheInterface',
        'factory' => ['@memcache.factory', 'get'],
        # Actual cache bin to use for the container cache.
        'arguments' => ['container'],
      ],
      # Define a custom cache tags invalidator for the bootstrap container.
      'cache_tags_provider.container' => [
        'class' => 'Drupal\memcache\Cache\TimestampCacheTagsChecksum',
        'arguments' => ['@memcache.timestamp.invalidator.tag'],
      ],
      'cache.container' => [
        'class' => 'Drupal\memcache\MemcacheBackend',
        'arguments' => [
          'container',
          '@memcache.backend.cache.container',
          '@cache_tags_provider.container',
          '@memcache.timestamp.invalidator.bin',
        ],
      ],
    ],
  ];

  // https://git.drupalcode.org/project/memcache/blob/8.x-2.x/README.txt
  if (class_exists('Memcached', FALSE)) {
    $settings['memcache']['options'] = [
      \Memcached::OPT_COMPRESSION => TRUE,
      \Memcached::OPT_DISTRIBUTION => \Memcached::DISTRIBUTION_CONSISTENT,
    ];
  }
  if (\Drupal::hasService('cache.backend.memcache')) {
    $default_cache = 'cache.backend.memcache';

    // Cache bin settings.
    $settings['cache']['default'] = $default_cache;
    $settings['cache']['bins']['bootstrap'] = $default_cache;
    $settings['cache']['bins']['config'] = $default_cache;
    $settings['cache']['bins']['container'] = $default_cache;
    $settings['cache']['bins']['data'] = $default_cache;
    $settings['cache']['bins']['default'] = $default_cache;
    $settings['cache']['bins']['discovery'] = $default_cache;
    $settings['cache']['bins']['dynamic_page_cache'] = $default_cache;
    $settings['cache']['bins']['entity'] = $default_cache;
    $settings['cache']['bins']['library'] = $default_cache;
    $settings['cache']['bins']['menu'] = $default_cache;
    //$settings['cache']['bins']['migrate'] = $default_cache;
    $settings['cache']['bins']['render'] = $default_cache;
    $settings['cache']['bins']['toolbar'] = $default_cache;
    $settings['cache']['bins']['page'] = $default_cache;
  }
}
