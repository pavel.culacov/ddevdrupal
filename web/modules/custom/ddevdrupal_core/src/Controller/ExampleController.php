<?php
namespace Drupal\ddevdrupal_core\Controller;

use Drupal\Core\Controller\ControllerBase;

class ExampleController extends ControllerBase
{
  public function index(){
    return [
      '#markup' => $this->t("translate me")
    ];
  }
}
